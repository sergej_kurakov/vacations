package org.example;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.camunda.bpm.client.ExternalTaskClient;
import org.example.dto.VacationDto;
import org.example.serializer.KafkaJsonSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Properties;

import static java.lang.Long.parseLong;

public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        Map<String, String> environment = System.getenv();
        Properties properties = new Properties();
        properties.setProperty(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                environment.getOrDefault("KAFKA_BOOTSTRAP_SERVERS", "kafka-service:9091"));
        properties.setProperty(
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class.getName());
        properties.setProperty(
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                KafkaJsonSerializer.class.getName());
        KafkaProducer<String, VacationDto> producer = new KafkaProducer<>(properties);
        String kafkaTopic = environment.get("KAFKA_TOPIC");
        ExternalTaskClient.create()
                .baseUrl(environment.getOrDefault("CAMUNDA_ENGINE_URL", "http://camunda:8080/engine-rest"))
                .asyncResponseTimeout(parseLong(environment.getOrDefault("CAMUNDA_ASYNC_RESPONSE_TIMEOUT", "10000")))
                .build()
                .subscribe(environment.get("CAMUNDA_TOPIC"))
                .lockDuration(parseLong(environment.getOrDefault("CAMUNDA_LOCK_DURATION", "10000")))
                .handler((task, taskService) -> {
                    try {
                        VacationDto dto = new VacationDto();
                        dto.setType(task.getVariable("type"));
                        dto.setComment(task.getVariable("comment"));
                        dto.setDeputy(task.getVariable("deputy"));
                        dto.setStartDate(task.getVariable("startDate"));
                        dto.setEndDate(task.getVariable("endDate"));
                        dto.setResolution(task.getVariable("resolution"));
                        producer.send(new ProducerRecord<>(kafkaTopic, task.getProcessInstanceId(), dto)).get();
                        taskService.complete(task);
                    } catch (Exception exception) {
                        LOGGER.error("Не удалось завершить задачу", exception);
                        throw new RuntimeException(exception);
                    }
                })
                .open();
    }
}
