package org.example.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serializer;

public class KafkaJsonSerializer<T> implements Serializer<T> {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Override
    public byte[] serialize(String topic, Object data) {
        try {
            return OBJECT_MAPPER.writeValueAsBytes(data);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }
}
