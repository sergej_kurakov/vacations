group = "org.example"
version = "1.0-SNAPSHOT"

plugins {
    java
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.camunda.bpm:camunda-external-task-client:7.16.0")
    implementation("javax.xml.bind:jaxb-api:2.3.1")
    implementation("org.apache.kafka:kafka-clients:3.1.0")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.13.1")
    implementation("org.apache.logging.log4j:log4j-api:2.17.1")
    implementation("org.slf4j:slf4j-simple:1.7.36")
}

tasks.withType<Jar> {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE

    manifest {
        attributes["Main-Class"] = "org.example.Application"
    }

    from(configurations.compileClasspath.map { config -> config.map { if (it.isDirectory) it else zipTree(it) } })
}
