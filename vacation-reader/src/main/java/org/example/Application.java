package org.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.camunda.bpm.client.ExternalTaskClient;
import org.example.dto.VacationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static java.lang.Long.parseLong;

public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static void main(String[] args) {
        Map<String, String> environment = System.getenv();
        Properties properties = new Properties();
        properties.setProperty(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                environment.getOrDefault("KAFKA_BOOTSTRAP_SERVERS", "kafka-service:9091"));
        properties.setProperty(
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class.getName());
        properties.setProperty(
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                ByteArrayDeserializer.class.getName());
        properties.setProperty(
                ConsumerConfig.GROUP_ID_CONFIG,
                environment.getOrDefault("KAFKA_GROUP_ID", "group_id"));
        KafkaConsumer<String, byte[]> consumerForApproved = new KafkaConsumer<>(properties);
        consumerForApproved.subscribe(List.of(
                environment.getOrDefault("KAFKA_TOPIC_FOR_APPROVED", "approved")));
        KafkaConsumer<String, byte[]> consumerForNotApproved = new KafkaConsumer<>(properties);
        consumerForNotApproved.subscribe(List.of(
                environment.getOrDefault("KAFKA_TOPIC_FOR_NOT_APPROVED", "notapproved")));
        Duration pollTimeout = Duration.ofMillis(parseLong(environment.getOrDefault("KAFKA_TIMEOUT", "1000")));
        ExternalTaskClient.create()
                .baseUrl(environment.getOrDefault("CAMUNDA_ENGINE_URL", "http://camunda:8080/engine-rest"))
                .asyncResponseTimeout(parseLong(environment.getOrDefault("CAMUNDA_ASYNC_RESPONSE_TIMEOUT", "10000")))
                .build()
                .subscribe(environment.getOrDefault("CAMUNDA_RESULTS_TOPIC", "results"))
                .lockDuration(parseLong(environment.getOrDefault("CAMUNDA_LOCK_DURATION", "10000")))
                .handler((task, taskService) -> {
                    try {
                        ConsumerRecords<String, byte[]> approved = consumerForApproved.poll(pollTimeout);
                        if (!approved.isEmpty()) {
                            VacationDto dto = MAPPER.readValue(approved.iterator().next().value(), VacationDto.class);
                            LOGGER.info("Отпуск c {} по {} согласован, резолюция: {}",
                                    dto.getStartDate(), dto.getEndDate(), dto.getResolution());
                            consumerForApproved.commitSync();
                            taskService.complete(task);
                        }
                        ConsumerRecords<String, byte[]> notApproved = consumerForNotApproved.poll(pollTimeout);
                        if (!notApproved.isEmpty()) {
                            VacationDto dto = MAPPER.readValue(notApproved.iterator().next().value(), VacationDto.class);
                            LOGGER.info("Отпуск c {} по {} не согласован, резолюция: {}",
                                    dto.getStartDate(), dto.getEndDate(), dto.getResolution());
                            consumerForNotApproved.commitSync();
                            taskService.complete(task);
                        }
                    } catch (Exception exception) {
                        LOGGER.error("Не удалось завершить задачу", exception);
                        throw new RuntimeException(exception);
                    }
                })
                .open();
    }
}
