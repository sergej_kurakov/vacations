Образы приложений для чтения и записи данных в Apache Kafka залиты на Docker Hub (вероятно, необходимо залогиниться в Docker Hub для просмотра, сами образы находятся в публичном доступе):

- https://hub.docker.com/repository/docker/zeus1994/vacation-reader
- https://hub.docker.com/repository/docker/zeus1994/vacation-writer-notapproved
- https://hub.docker.com/repository/docker/zeus1994/vacation-writer-approved

Для развёртывания системы в среде Kubernetes необходимо выполнить следующие шаги:

1. Создать пространство имён:

kubectl create -f ./vacation.json

2. Развернуть Apache Camunda:

kubectl create -f ./camunda.yml -n vacation

3. Развернуть Apache Kafka:

kubectl create -f ./kafka.yml -n vacation

4. Равзернуть приложения:

- kubectl create -f ./vacation-writer-approved.yml -n vacation
- kubectl create -f ./vacation-writer-notapproved.yml -n vacation
- kubectl create -f ./vacation-reader.yml -n vacation

Файл бизнес-процесса:

https://gitlab.com/sergej_kurakov/vacations/-/blob/main/vacation.bpmn

После успешного развёртывания контернеров можно будет зайти на начальную страницу Apache Camunda:

http://localhost:30000/camunda-welcome/index.html

Запустить процесс можно через Camunda Tasklist. После заполнения формы в Tasklist должна появиться задача на согласование отпуска.

Приложения vacation-writer-approved и vacation-writer-notapproved записывают в соответствующий топик кафки информацию о согласовании/не согласовании отпуска.

Приложение vacation-reader читает данные из топиков и выводит результат согласования/не согласования. Реузльтат выводится в логи, т. к. для вывода в Cockpit необходимо вносить изменения бизнес-процесс, например, добавить в конец User Task - в этом случае результат согласования появится в Tasklist. Для указанного в требованиях бизнес-процесса можно реализовать разве что отправку данных во внешнюю систему либо по email, на текущий момент реализован простейший вариант.

Логи можно посмотреть через интерфейс Kubernetes в приложении vacation-reader, должно быть сообщение вида "Отпуск с {} по {} согласован/не согласован".

